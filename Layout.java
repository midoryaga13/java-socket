package layout_manager;

import java.awt.*;

import javax.swing.JFrame;
import javax.swing.JLabel;

public class Layout{
	
	static public void main(String arg []) {
		JFrame fen;
		Dimension dimEcran;
		JLabel label;
		fen = new JFrame();
		fen.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		fen.setBounds(50, 100, 300, 500);
		dimEcran = fen.getToolkit().getScreenSize();
		System.out.println(dimEcran);
		label = new JLabel("Taille de l'�cran : " +dimEcran);
		fen.getContentPane().add(label);
		fen.pack();
	}

}
