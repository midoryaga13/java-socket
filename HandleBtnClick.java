package layout_manager;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.*;

public class HandleBtnClick implements ActionListener {
	
	static int compteur = 0;
	private JButton btn;
	private static String url1 = "logo1.png";
	private static String url2 = "logo2.jpg";
	private static String url3 = "logo3.jpg";
	private static ImageIcon[] TABIMAGES = {new ImageIcon(url1), new ImageIcon(url2), new ImageIcon(url3)};
	
	HandleBtnClick(JButton lebtn) {
		btn = lebtn;
	}
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		JFrame fen = new JFrame("Les images");
		fen.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		fen.setBounds(50, 100, 300, 500);
		JButton lebtn = new JButton(TABIMAGES[compteur]);
		HandleBtnClick instance = new HandleBtnClick(lebtn);
		lebtn.addActionListener(instance);
		fen.getContentPane().add(lebtn);
		fen.pack();
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		compteur = (compteur + 1) % TABIMAGES.length;
		btn.setIcon(TABIMAGES[compteur]);
	}
	

}
