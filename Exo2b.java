package layout_manager;

import java.awt.*;
import javax.swing.*;

public class Exo2b {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		JFrame fen = new JFrame("NombreMystere");
		fen.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		JButton btnFin = new JButton("FIN");
		JButton btnBegin = new JButton("COMMENCER");
		JLabel labelResult;
		JLabel lbCoups;
		labelResult = new JLabel("Trouver un nombre entre 0 et 99");
		lbCoups = new JLabel("0");
		JTextField txtSaisie;
		txtSaisie = new JTextField("  ");
		fen.setBounds(50, 100, 300, 500);
		Container cont = fen.getContentPane();
//		cont.setLayout(new BorderLayout());
//		cont.add(btnFin, BorderLayout.EAST);
//		cont.add(btnBegin, BorderLayout.WEST);
//		cont.add(txtSaisie, BorderLayout.CENTER);
//		cont.add(labelResult, BorderLayout.NORTH);
//		cont.add(lbCoups, BorderLayout.SOUTH);
//		cont.setLayout(new GridLayout(3,2));
//		cont.setLayout(new BoxLayout(cont, 0));
		GridBagLayout gb = new GridBagLayout();
		GridBagConstraints c = new GridBagConstraints();
		cont.setLayout(gb);
		c.gridx = 1;
		c.gridy = 0;
		c.gridwidth = 2;
		cont.add(btnFin, c);
		c.gridx = 0;
		c.gridy = 2;
		c.gridwidth = 6;
		cont.add(btnBegin, c);
		c.gridx = 0;
		c.gridy = 2;
		cont.add(txtSaisie, c);
		c.gridx = 0;
		c.gridy = 4;
		cont.add(labelResult, c);
		c.gridx = 0;
		c.gridy = 5;
		cont.add(lbCoups, c);
		fen.pack();
		fen.show();
	}

}
