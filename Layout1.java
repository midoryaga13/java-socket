package layout_manager;

import java.awt.Dimension;

import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;

public class Layout1 {

	static public void main(String arg []) {
		JFrame fen;
		JLabel label;
		fen = new JFrame();
		fen.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		fen.setBounds(50, 100, 300, 500);
		String url = "legume_img.jpg";
		ImageIcon icon1 = new ImageIcon(url);
		label = new JLabel(icon1);
		fen.getContentPane().add(label);
		fen.setLocation(700, 200);
		fen.pack();
		fen.show();
	}

}
